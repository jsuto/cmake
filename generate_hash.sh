#!/bin/bash

set -o nounset
set -o errexit
set -o pipefail

echo "#define GIT_HASH \"$( git log -1 --format=oneline | cut -f1 -d ' ' )\""
