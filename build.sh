#!/bin/bash

set -o nounset
set -o errexit
set -o pipefail

SHARED_LIB="${SHARED_LIB:-ON}"
USE_MODE="${1:-MODE_SUBDIR}"
CMD_ARGS=()

echo "USE_MODE: ${USE_MODE}"

[[ "$USE_MODE" == "MODE_SUBDIR" ]] && CMD_ARGS+=( "-DSHARED_LIB=${SHARED_LIB}" )

CMD_ARGS+=( "-DUSE_MODE=${USE_MODE}" . )

cmake "${CMD_ARGS[@]}"

make
