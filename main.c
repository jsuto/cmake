#include "version.h"
#include "msg.h"
#include <stdio.h>

int main()
{
    printf("VERSION: %s\nHASH: %s\n", PROJ_VERSION, message() );
    return 0;
}
